{{ config(materialized='table' ,schema='VENKAT') }}

select query_id ,query_text ,table_name ,view_name ,exec_date
    from {{ ref('queries_and_views') }}
union
select query_id ,query_text ,table_name ,view_name ,exec_date
    from {{ ref('queries_and_tables') }}
