{{ config(materialized='ephemeral' ,schema='VENKAT') }}

select att.table_catalog ,att.table_schema ,att.table_name 
        ,av.table_name as view_name ,av.view_definition
        ,att.table_catalog_id ,att.table_schema_id  
from {{ref('active_base_tables')}} att
    JOIN {{ ref('active_views') }} av ON 
        av.table_catalog_id = att.table_catalog_id 
        and av.table_schema_id = att.table_schema_id 
where UPPER(av.view_definition) like  ('%' || UPPER(att.table_name) || '%')    
