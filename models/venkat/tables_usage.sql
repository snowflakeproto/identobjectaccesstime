{{ config(materialized='view' ,schema='VENKAT') }}

select table_name ,count(distinct query_id) usage_count ,max(exec_date) last_used
    from {{ ref('queries_with_metadata') }}
    group by table_name
    order by last_used asc ,usage_count asc

