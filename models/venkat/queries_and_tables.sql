{{ config(materialized='ephemeral' ,schema='VENKAT') }}

select tbl.table_name ,tbl.view_name 
        ,qry.query_id ,qry.query_text ,qry.exec_date
    from {{ ref('deduplicated_queries') }} qry 
        ,{{ ref('active_tables') }} tbl 
    where qry.query_text  like  ('%' || tbl.table_name || '%') 
