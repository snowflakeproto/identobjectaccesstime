{{ config(materialized='table' ,schema='VENKAT') }}

select table_catalog ,table_schema ,view_definition ,table_name
        ,table_catalog_id ,table_schema_id
      from {{source('VENKAT','ACCOUNT_USAGE_VIEWS')}}
      where deleted is null