{{ config(materialized='table' ,schema='VENKAT') }}



select query_id, UPPER(query_text) as query_text ,database_name ,schema_name ,query_type 
    ,database_id ,schema_id ,to_date(start_time) exec_date
    , count(*) dummy_cnt //found that there could be double entry for the same query_id, with same exec time , user, query etc.. hence this count is added to deduplicate
 from {{ source('VENKAT','ACCOUNT_USAGE_QUERY_HISTORY') }}
 where warehouse_size is not null and END_TIME is not null
    and database_name not in ('SNOWFLAKE' ,'SNOWFLAKE_SAMPLE_DATA')
 group by query_id, query_text ,database_name ,schema_name ,query_type 
    ,database_id ,schema_id ,exec_date