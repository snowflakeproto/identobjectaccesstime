{{ config(materialized='table' ,schema='VENKAT') }}

select act.table_catalog ,act.table_schema ,act.table_name
        ,usg.last_used ,usg.usage_count
    from {{ ref('active_tables') }} act 
        LEFT OUTER JOIN {{ ref('tables_usage') }} usg ON
            act.table_name = usg.table_name
    order by last_used asc ,usage_count asc