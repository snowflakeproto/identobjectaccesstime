{{ config(materialized='table' ,schema='VENKAT') }}


select  att.table_catalog ,att.table_schema ,att.table_name 
             ,tiv.view_name ,tiv.view_definition 
             ,att.table_catalog_id ,att.table_schema_id
            ,count(*) dummy_cnt 
          from {{ ref('active_base_tables') }} att
          LEFT OUTER JOIN {{ ref('tables_definedin_views') }} tiv 
                  ON tiv.table_catalog_id = att.table_catalog_id and tiv.table_schema_id = att.table_schema_id 
                      and tiv.table_name = att.table_name
          group by att.table_catalog ,att.table_schema ,att.table_name 
                    ,tiv.view_name ,tiv.view_definition
                    ,att.table_catalog_id ,att.table_schema_id