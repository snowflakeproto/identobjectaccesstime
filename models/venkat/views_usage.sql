{{ config(materialized='view' ,schema='VENKAT') }}

select view_name ,count(distinct query_id) usage_count ,max(exec_date) last_used
    from {{ ref('queries_with_metadata') }}
    where view_name is not null
    group by view_name
    order by last_used asc ,usage_count asc