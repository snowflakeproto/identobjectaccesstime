{{ config(materialized='table' ,schema='VENKAT') }}

select table_catalog ,table_schema ,table_name
        ,table_catalog_id ,table_schema_id ,table_type
      from {{source('VENKAT','ACCOUNT_USAGE_TABLES')}}
      where deleted is null
        and table_type = 'BASE TABLE'
