# Housekeeping in Snowflake : Identifying least used tables & views

After months of Snowflake adoptions, it would be apparent that a quantitative
amount of tables from various sources would have been loaded into Snowflake. Apart
from this, tables/views would get also get created as part of various data pipeline.

Though storage and the compute is cheap; it would be beneficial to maintain some 
housekeeping of the various objects that gets created in Snowflake. The result of of housekeeping would involve some of the following :

* List out the tables/views that are least used.
* Identify when the tables/views accessed and by whom ?
* List out the tables that can be dropped ?
* Are there any tables that are loaded but not used ?

Interviewing the various Snowflake users and formulating the result is what would come to mind, but this process is long and not reliable. Some users would have moved onto other parts of the organizations. There could also be "Ghost tables" that gets loaded from the sources, but it does not get used in any data pipelines.

Another way to approach this problem is using the information present in the  SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY table. If this sounds interesting follow along, as I demonstrate.

---
**NOTE:**

I am using my sandbox environment, which is used for learning ,training and other non-critical pruposes. Hence conventions
such as sql coding , object (tables, views etc..) naming convention is not followed.

---

## Solution
In this section, i will walk through the implementation at a high level. The sql
code used for the implementation is later sections.

### Overview
The [SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY](https://docs.snowflake.com/en/sql-reference/account-usage/query_history.html) view contains the various queries issued by users against any object. Every SQL executions gets logged and is available through this view. The actual statement issued is available in the 'QUERY_TEXT' column.

Here is a sample query that was issued:
```sql
    SELECT count('x') AS count
        FROM (
            SELECT foo.SUBSCRIPTION_ID, foo.ITEMS, foo.__SDC_PRIMARY_KEY, 
                foo.DATASOURCE, foo._SDC_SEQUENCE, foo. _SDC_RECEIVED_AT, foo._SDC_BATCHED_AT, foo._SDC_TABLE_VERSION
            FROM (
                SELECT *,
                      row_number () OVER (PARTITION BY STAGING_DATA_13_LOADER_SNOW10_11761_72FD67BD_4453_4EC1_8E05_83351F9F767B.__SDC_PRIMARY_KEY ORDER BY _sdc_sequence DESC) AS rnk
                 FROM STAGING_DATA_LOADER ) AS foo
                WHERE rnk = 1
                )
```

To determine which table is used in the query, we can do an LIKE operation on the query_text with list of tables ,from the view  SNOWFLAKE.ACCOUNT_USAGE.TABLES. To determine the view used, we can follow a similar approach with using the view SNOWFLAKE.ACCOUNT_USAGE.VIEWS.  

To determine the underlying tables that are involved in the view, you can use the 'VIEW_DEFINITION' column.  

once the queries has been associated with the tables & views, using the query time ,no of queries issued etc.. we can then answer the above mentioned housekeeping queries (repeated below):
* List out the tables/views that are least used.
* Identify when the tables/views accessed and by whom ?
* List out the tables that can be dropped ?
* Are there any tables that are loaded but not used ?

### Pre-requisite
The solution involves with views from the SNOWFLAKE.ACCOUNT_USAGE, hence in order to do this, I recomend the following steps:

- Define a custom role (ex: HOUSEKEEPER )
- Grant 'imported privileges' to this role, refer : [Enabling Account Usage for Other Roles](https://docs.snowflake.com/en/sql-reference/account-usage.html#enabling-account-usage-for-other-roles)
- Create a seperate schema (ex: HOUSEKEEPING), the HOUSEKEEPER role should be able to tables.

---
**NOTE: WARNING**

* The 'QUERY_TEXT' column contains the actual query issued by various users and processes, It is possible that the query issued contains sensitive data. Hence 
I recomend that the 'HOUSEKEEPER' role be given only to specific people.

* Limit access to the schema 'HOUSEKEEPING', as this will contain copies of the queries view.
---

### Logic Flow

Here is the screenshot of the sequence of steps

![](docs/images/datalogic_sequence.jpg)

Below is explanation of the various stages, the stage names are reflected accordingly in the steps.

1. Create a copy of the following tables in the schema 'HOUSEKEEPING'
    - SNOWFLAKE.ACCOUNT_USAGE.TABLES
    - SNOWFLAKE.ACCOUNT_USAGE.VIEWS
    - SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY
   
   I recomend taking a snapshot copying, depending on conditions, the query data reteival might be slower when issuing against these views.

2. [Stage: active_base_tables] Get the list of active tables. Active tables mean that they are not deleted.
3. [Stage: active_views & tables_definedin_views] Get the list of active views and identify the tables used.
4. [Stage: active_tables] Create a temp table 'active_tables' which will contain the result of #2 & #4.
5. [Stage: deduplicated_queries] Deduplicate the queries found in the SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY. Typically the query enteries are unique, there has been occassions where i have come across 2 records which are identical repetation in the table.  
6. [Stage: queries_and_views, queries_and_tables ,queries_with_metadata] Using the query_text we start associating the tables to determine the list of tables used. We store the result in the table 'queries_with_metadata'.
7. [Stage: views_usage ,tables_usage ,tables_not_used] We can now issue different queries to get the various housekeeping related answers.

Additional views can also be constructed in the future. For ex:
- Which tables are inter-related ?
- Which tables are the most active ?
- Who will get affected if a table was accidentally dropped ?
etc....

As mentioned earlier, the implementation code is presented in later sections.

### Result
Below are some screenshots of results in my sandbox environment

#### VIEWS_USAGE
This is the list of views and when they were last used ,based of queries issued against them.
![](docs/images/views_usage.jpg)

#### TABLES_USAGE
This is the list of tabless and when they were last used ,based of queries issued against them.
![](docs/images/tables_usage.jpg)

#### TABLES_NOT_USED
This is list of tables, which were used in queries, along with information on the database & schema. Based of the last_used/usage count you can determine which tables to retain and which tables to drop.
![](docs/images/tables_not_used.jpg)


### Current Limitations
The code/logic does not solve all kinds of scenario & environment, expect some modifications based on your environment. Here are some
limitations i could think of currently

##### Snowflake Usage
As mentioned this walkthru was done using my sandbox environment. Its not a very active environment, your mileage will varry. The variotion is based of warehosue used to do the above processing.

##### Exact Database and Schema
In your environment, it is very very highly likely that there are multiple table/view definitions across different schema/databases etc.. Hence filtering down to the exact database/schema in which the said table/view present will require some work.

_________________

## Implementation Code
The code is implemented using purely SQL and all executed within the Snowflake environment. I have this code implemented as a DBT project in my 
   **[GITLAB REPO : identobjectaccesstime](https://gitlab.com/snowflakeproto/identobjectaccesstime/-/tree/master)**

For convienience i have captured the sql statement as below, you can adopt the below approach too if you are not aware of DBT. Feel free to
rename the table accordingly. I am not perfect in naming things. 

### Step 1: Account usage views

As mentioned , we start of making a copy of the various views from the SNOWFLAKE.ACCOUNT_USAGE schema.

```sql
create or replace transient table TEST_DB.VENKAT.ACCOUNT_USAGE_TABLES as
  select * from SNOWFLAKE.ACCOUNT_USAGE.TABLES;

create or replace transient table TEST_DB.VENKAT.ACCOUNT_USAGE_VIEWS as
  select * from SNOWFLAKE.ACCOUNT_USAGE.VIEWS;

create or replace transient table TEST_DB.VENKAT.ACCOUNT_USAGE_QUERY_HISTORY as
  select * from SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY;

```

### Stage: active_base_tables
```sql
create or replace transient table active_base_tables as
    select table_catalog ,table_schema ,table_name
        ,table_catalog_id ,table_schema_id ,table_type
        from ACCOUNT_USAGE_TABLES
        where deleted is null
            and table_type = 'BASE TABLE'
```

### Stage: active_views
```sql
create or replace transient table active_views as
    select table_catalog ,table_schema ,view_definition ,table_name
        ,table_catalog_id ,table_schema_id
    from ACCOUNT_USAGE_VIEWS
    where deleted is null
```

### Stage: tables_definedin_views
```sql

create or replace transient table tables_definedin_views as
    select att.table_catalog ,att.table_schema ,att.table_name 
        ,av.table_name as view_name ,av.view_definition
        ,att.table_catalog_id ,att.table_schema_id  
    from active_base_tables att
        JOIN active_views av ON 
            av.table_catalog_id = att.table_catalog_id 
            and av.table_schema_id = att.table_schema_id 
    where UPPER(av.view_definition) like  ('%' || UPPER(att.table_name) || '%')    
```

### Stage: active_tables
```sql

create or replace transient table active_tables as
select  att.table_catalog ,att.table_schema ,att.table_name 
             ,tiv.view_name ,tiv.view_definition 
             ,att.table_catalog_id ,att.table_schema_id
            ,count(*) dummy_cnt 
          from active_base_tables att
          LEFT OUTER JOIN tables_definedin_views tiv 
                  ON tiv.table_catalog_id = att.table_catalog_id and tiv.table_schema_id = att.table_schema_id 
                      and tiv.table_name = att.table_name
          group by att.table_catalog ,att.table_schema ,att.table_name 
                    ,tiv.view_name ,tiv.view_definition
                    ,att.table_catalog_id ,att.table_schema_id
```

### Stage: deduplicated_queries
```sql

create or replace transient table deduplicated_queries as
    select query_id, UPPER(query_text) as query_text ,database_name ,schema_name ,query_type 
            ,database_id ,schema_id ,to_date(start_time) exec_date
            , count(*) dummy_cnt 
    from ACCOUNT_USAGE_QUERY_HISTORY
    where warehouse_size is not null and END_TIME is not null
            and database_name not in ('SNOWFLAKE' ,'SNOWFLAKE_SAMPLE_DATA')
    group by query_id, query_text ,database_name ,schema_name ,query_type 
        ,database_id ,schema_id ,exec_date
```

### Stage: queries_and_views
```sql

create or replace transient table queries_and_views as
select tbl.table_name ,tbl.view_name 
            ,qry.query_id ,qry.query_text ,qry.exec_date
        from deduplicated_queries qry 
            ,active_tables tbl 
        where qry.query_text  like  ('%' || tbl.view_name || '%')  
```

### Stage: queries_and_tables
```sql

create or replace transient table queries_and_views as
select tbl.table_name ,tbl.view_name 
        ,qry.query_id ,qry.query_text ,qry.exec_date
    from deduplicated_queries qry 
        ,active_tables tbl 
    where qry.query_text  like  ('%' || tbl.table_name || '%') 
```

### Stage: queries_with_metadata
```sql

create or replace transient table queries_with_metadata as
select query_id ,query_text ,table_name ,view_name ,exec_date
    from queries_and_views
union
select query_id ,query_text ,table_name ,view_name ,exec_date
    from queries_and_tables
```

### Stage: views_usage
```sql

create or replace transient table views_usage as
select view_name ,count(distinct query_id) usage_count ,max(exec_date) last_used
    from queries_with_metadata
    where view_name is not null
    group by view_name
    order by last_used asc ,usage_count asc
```

### Stage: tables_usage
```sql

create or replace transient table tables_usage as
select table_name ,count(distinct query_id) usage_count ,max(exec_date) last_used
    from queries_with_metadata
    group by table_name
    order by last_used asc ,usage_count asc
```

### Stage: tables_not_used
```sql

create or replace transient table tables_not_used as
select act.table_catalog ,act.table_schema ,act.table_name
        ,usg.last_used ,usg.usage_count
    from active_tables act 
        LEFT OUTER JOIN tables_usage usg ON
            act.table_name = usg.table_name
    order by last_used asc ,usage_count asc
```